// 1. What directive is used by Node.js in loading the modules it needs?

'require' directive

// 2. What Node.js module contains a method for server creation?

http.createServer(function (request, response) {}

// 3. What is the method of the http object responsible for creating a server using Node.js?

const server = http.createServer((request, response) => {})

// 4. What method of the response object allows us to set status codes and content types?
	
response.writeHead(200, {'Content-Type': 'text/plain' });
response.end('Example');

// 5. Where will console.log() output its contents when run in Node.js?

// In GitBash(terminal window)

// 6. What property of the request object contains the address's endpoint?

(request.url == '/example') 
